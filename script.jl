using PowerModels
using Ipopt

using DataFrames
using CSV

include("aly/renewables.jl")

file = "matpower/case5.m"
dic = PowerModels.parse_matpower(file)

network_nodes = CSV.read("RE-Europe_dataset_package/Metadata/network_nodes.csv",DataFrame)
network_edges = CSV.read("RE-Europe_dataset_package/Metadata/network_edges.csv",DataFrame)
generator_info = CSV.read("RE-Europe_dataset_package/Metadata/generator_info.csv",DataFrame)
load_signal = CSV.read("RE-Europe_dataset_package/Nodal_TS/load_signal.csv",DataFrame)

# df = CSV.read(generator_info, DataFrame)

### sous partie bus
vmin=0
vmax=2
bus=Dict{String,Any}()
for row in eachrow(network_nodes)
	dict_temp=Dict("vmin"=>vmin,"vmax"=>vmax,"base_kv"=>row.voltage,"bus_type"=>2,"index"=>row.ID,"source_id"=>Any["bus",row.ID],"bus_i"=>row.ID)
	bus[string(row.ID)]=dict_temp
end

### sous partie generator
model=2
qmin=-Inf
qmax=Inf
pmin=0
gen=Dict{String,Any}()
for row in eachrow(generator_info)
	dict_temp=Dict("shutdown"=>0.0,"startup"=>0,"model"=>model,"cost"=>[0,row.lincost],"qmin"=>qmin,"qmax"=>qmax,"pmin"=>pmin,"pmax"=>row.capacity,"gen_status"=>1,
							"gen_bus"=>row.origin,"source_id"=>Any["gen",row.ID],"index"=>row.ID)
	gen[string(row.ID)]=dict_temp 	
end

### sous partie edge
b=0
angmin=-1.0472
angmax=1.0472
r=0

edges=Dict{String,Any}()
for i in 1:size(network_edges,1)
	row=network_edges[i,:]
	dict_temp=Dict("f_bus"=>row.fromNode,"t_bus"=>row.toNode,"br_r"=>r,"br_x"=>row.X,
						"angmin"=>angmin,"angmax"=>angmax,"br_status"=>1,"index"=>i,"tap"=>1.0,"shift"=>0.0,
						"g_to"=>0.0,"g_fr"=>0.0,"b_to"=>0.0,"b_fr"=>0.0)
	edges[string(i)]=dict_temp
end


### sous partie load

load=Dict{String,Any}()
for i in 2:size(load_signal,2)
	dict_temp=Dict("load_bus"=>parse(Int,(names(load_signal)[i])),"source_id"=>Any["bus",parse(Int,names(load_signal)[i])],
							"status"=>1,"pd"=>load_signal[1,i],"qd"=>0,"index"=>i)
	load[string(i)]=dict_temp
end

dic_general=Dict{String,Any}();
dic_general["bus"]=bus;
dic_general["gen"]=gen;
dic_general["branch"]=edges;
dic_general["load"]=load;
dic_general["shunt"]=Dict{String,Any}();
dic_general["storage"]=Dict{String,Any}();
dic_general["switch"]=Dict{String,Any}();
dic_general["dcline"]=Dict{String,Any}();
dic_general["per_unit"]=true;
dic_general["baseMVA"]=100;

max_gen=maximum(parse.(Int,collect(keys(dic_general["gen"]))))

# pm=PowerModels.instantiate_model(dic_general,PowerModels.ACPPowerModel,PowerModels.build_opf);
# pm_ex=PowerModels.instantiate_model(dic,PowerModels.ACPPowerModel,PowerModels.build_opf);

#run_opf(dic_general,PowerModels.ACPPowerModel,Ipopt.Optimizer)

#Ajout des énergies renouvelables.

solar, wind = get_renewables(layout_type = "Uniform", model = "COSMO",);

for i in 2:size(solar,2)-1
	bus_id=names(solar)[i]
	dict_temp=Dict("shutdown"=>0.0,"startup"=>0,"model"=>model,"cost"=>[0,0],"qmin"=>qmin,"qmax"=>qmax,"pmin"=>pmin,"pmax"=>solar[1,bus_id],"gen_status"=>1,
							"gen_bus"=>parse(Int,bus_id),"source_id"=>Any["gen",i+max_gen],"index"=>i+max_gen);
	gen[string(i+max_gen)]=dict_temp
end

max_gen_plus_solar=maximum(parse.(Int,collect(keys(dic_general["gen"]))))

for i in 2:size(wind,2)-1
	bus_id=names(wind)[i]
	dict_temp=Dict("shutdown"=>0.0,"startup"=>0,"model"=>model,"cost"=>[0,0],"qmin"=>qmin,"qmax"=>qmax,"pmin"=>pmin,"pmax"=>wind[1,bus_id],"gen_status"=>1,
							"gen_bus"=>parse(Int,bus_id),"source_id"=>Any["gen",i+max_gen_plus_solar],"index"=>i+max_gen_plus_solar);
	gen[string(i+max_gen_plus_solar)]=dict_temp
end

result = run_opf(dic_general,PowerModels.ACPPowerModel,Ipopt.Optimizer)

# tot_pd=0
# for (key, value) in dic_general["load"]
# 	global tot_pd+=value["pd"];
# end

# tot_pmax=0
# for (key, value) in dic_general["gen"]
# 	global tot_pmax+=value["pmax"];
# end
