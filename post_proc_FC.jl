using CSV
using DataFrames
using PowerModels
using Ipopt
using StatsPlots
using JLD
using InlineStrings
using MathOptInterface

include("fonctions/renewables.jl")
include("fonctions/pre_processing.jl")
include("fonctions/visualization.jl")

## Chargement de la table de correspondance
table_correspondance = load(
	"resultats/OPF_effectif/table_correspondance.jld", 
	"table_correspondance")
# Ajout des colonnes de resultats
for i in 0:90
	table_correspondance[:, string("resultat_FC_",i) ] = zeros(Bool, size(table_correspondance,1))
end


# Lecture des resultats
path = "resultats/OPF_FC/"
FC_folders = readdir(path) 

for fold in FC_folders[1:2]
	res_files = readdir(string(path, fold))
	nids = [parse(Int, file[4:end-4]) for file in res_files]
	firstnid = sort(nids)[1]
	for fc_hour in 0:90
		file = string("nid", firstnid+fc_hour, ".jld")
		full_path = string(path, fold, "/" ,file)

		local result = load(full_path, "result")
		table_correspondance[table_correspondance.nid .== firstnid+fc_hour, string("resultat_FC_", fc_hour)] .= (result["termination_status"] == LOCALLY_SOLVED) || (result["termination_status"] == OPTIMAL)
	end
end

table_correspondance