using DataFrames
using JLD
using MathOptInterface

include("fonctions/pre_processing.jl")
include("fonctions/renewables.jl")
include("fonctions/visualization.jl")

for i=1:10000
	nid = i
	filepath = string("resultats/OPF_effectif/nid",nid,".jld")
	result = load(filepath, "result")
	visualize_voltage_angle_position(result, string(nid))
end