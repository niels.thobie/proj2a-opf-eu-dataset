using Plots
using StatsPlots
using PlotlyJS

function visualize_prodcons(dict_in::Dict, result::Dict)

	active_gen = 0.0
	reactive_gen = 0.0
	active_cons = 0.0
	reactive_cons = 0.0

	for (key,value) in result["solution"]["gen"]
		active_gen += value["pg"]
		reactive_gen += value["qg"]
	end

	for (key,value) in dict_in["load"]
		active_cons += value["pd"]
		reactive_cons += value["qd"]
	end

	x = ["Active gen", "Active cons", "Reactive gen", "Reactive cons"]
	y = [active_gen, active_cons, reactive_gen, reactive_cons]

	Plots.bar(x,y)
end

function visualize_busprod(result::Dict)

	genids = []
	active = Number[]
	reactive = Number[]
	for (key, value) in result["solution"]["gen"]
		push!(genids, key)
		push!(active, value["pg"])
		push!(reactive, value["qg"])
	end

	Plots.bar(active)
end 

function visualize_branches(dict_in::Dict, result::Dict)

	active = Number[]
	for (key, value) in result["solution"]["branch"]
		push!(active, value["pf"])
	end
	Plots.bar(active)
end

function visualize_busprod_withandwith_ren(result1::Dict, result2::Dict)
	active_gen1=[]
	active_gen2=[]
	x=[]
	push!(x,keys(result1["solution"]["gen"]))
	k=0
	for (key,value) in result1["solution"]["gen"]
		k+=1
		(k>10) && break
		push!(active_gen1,value["pg"])
	end
	k=0
	for (key,value) in result1["solution"]["gen"]
		k+=1
		(k>10) && break
		push!(active_gen2,values(result2["solution"]["gen"][key]["pg"]))
	end
	StatsPlots.groupedbar([active_gen1 active_gen2])
end

function visualize_pmaxdiff(dict_in::Dict, result::Dict)
	bus = []
	diffs = Number[]

	for (key, value) in dict_in["gen"]
		diff = value["pmax"] - result["solution"]["gen"][key]["pg"]
		push!(bus, key)
		push!(diffs, diff)
	end

	Plots.bar(diffs)
end

function visualize_voltage(result::Dict)
	vr = Number[]
	vi = Number[]

	for (key, value) in result["solution"]["bus"]
		vm = value["vm"]
		va = value["va"]
		push!(vr, real(vm*exp(im*va)))
		push!(vi, imag(vm*exp(im*va)))
	end

	Plots.scatter(vr, vi, )
end

function visualize_voltage_angle_position(result::Dict,fileimg::String)
	network_nodes = CSV.read("RE-Europe_dataset_package/Metadata/network_nodes.csv",DataFrame)

	latitudes = Number[]
	longitudes = Number[]
	voltage_angles = Number[]

	for (key, value) in result["solution"]["bus"]
		df_row = network_nodes[network_nodes.ID .== parse(Int, key),:]
		push!(latitudes, df_row.latitude[1])
		push!(longitudes, df_row.longitude[1])
		push!(voltage_angles, value["va"])
	end

	trace = PlotlyJS.scatter(;
				x = longitudes, 
				y = latitudes, 
				mode = "markers",
				marker_color = voltage_angles, 
				marker_size = 5, 
				marker_colorscale = "Portland",
				marker_showscale = true,
				marker_colorbar_title = "Voltage angle",
				marker_cmin = -1.2,
				marker_cmax = 0.8,
				)
	fig=PlotlyJS.plot(trace)
	println(string("resultats/image_test/im",fileimg,".png"))
	# PlotlyJS.savefig(fig, string("resultats/image_test/im",fileimg,".png"); format = "png", height = 140, width = 330,)
	PlotlyJS.savefig(fig, string("resultats/image_test/im",fileimg,".png"))

end

function visualize_power_position(dict_in::Dict, result::Dict)
	network_nodes = CSV.read("RE-Europe_dataset_package/Metadata/network_nodes.csv",DataFrame)

	n = size(network_nodes, 1)
	df = DataFrame(:ID=>network_nodes.ID, :puissance=>fill(0.0, n), 
		:latitude=>fill(0.0,n), :longitude=>fill(0.0,n))

	for dfrow in eachrow(df)
		df_row = network_nodes[network_nodes.ID .== dfrow.ID,:]
		dfrow.latitude = df_row.latitude[1]
		dfrow.longitude = df_row.longitude[1]
	end

	for (k,v) in dict_in["load"]
		bus = v["load_bus"]
		ind = findfirst(df.ID .== bus)
		df[ind, :puissance] = df[ind, :puissance] - v["pd"]
	end

	for (k,v) in result["solution"]["gen"]
		bus = dict_in["gen"][k]["gen_bus"]
		ind = findfirst(df.ID .== bus)
		df[ind, :puissance] = df[ind, :puissance] + v["pg"]
	end

	trace = PlotlyJS.scatter(df;
				x = :longitude, 
				y = :latitude, 
				mode = "markers",
				marker_color = :puissance, 
				marker_cmin = -5,
				marker_cmax = 5,
				marker_size = 5, 
				marker_colorscale = "Bluered",
				marker_showscale = true,
				marker_colorbar_title = "Active power",
				)
	PlotlyJS.plot(trace)
end