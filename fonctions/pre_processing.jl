function create_dict(solar::DataFrameRow, wind::DataFrameRow,load_signal::DataFrameRow)
	# Créé le dictionnaire en entrée de l'opf

	network_nodes = CSV.read("RE-Europe_dataset_package/Metadata/network_nodes.csv",DataFrame)
	network_edges = CSV.read("RE-Europe_dataset_package/Metadata/network_edges.csv",DataFrame)
	generator_info = CSV.read("RE-Europe_dataset_package/Metadata/generator_info.csv",DataFrame)

	### sous partie bus
	vmin=0.9
	vmax=1.1
	bus=Dict{String,Any}()
	va = 0
	vm = 1
	for row in eachrow(network_nodes)
		dict_temp=Dict("vmin"=>vmin,"vmax"=>vmax,"base_kv"=>row.voltage,
			"bus_type"=>2,"index"=>row.ID,"source_id"=>Any["bus",row.ID],
			"bus_i"=>row.ID,"ref"=>1, "va"=>va, "vm"=>vm,
			"zone"=>1, "area"=>1, "name"=>row.name)
		bus[string(row.ID)]=dict_temp
	end
	bus["1"]["bus_type"]=3

	### sous partie generator
	model=2
	qmin=-Inf
	qmax=Inf
	pmin=0
	gen=Dict{String,Any}()
	for row in eachrow(generator_info)
		dict_temp=Dict("shutdown"=>0.0,"startup"=>0,"model"=>model,
								"cost"=>[row.lincost,0],"qmin"=>qmin,"qmax"=>qmax,
								"pmin"=>pmin/1,"pmax"=>row.capacity/1000,"gen_status"=>1,
								"gen_bus"=>row.origin,"source_id"=>Any["gen",row.ID],"index"=>row.ID, "ncost"=>2)
		gen[string(row.ID)]=dict_temp 
	end

	### sous partie edge
	b=0
	angmin=-1.0472
	angmax=1.0472
	r=0

	edges=Dict{String,Any}()
	for i in 1:size(network_edges,1)
		row=network_edges[i,:]
		if (row.limit == 0)
			dict_temp=Dict("f_bus"=>row.fromNode,"t_bus"=>row.toNode,"br_r"=>r,"br_x"=>row.X,
								"angmin"=>angmin,"angmax"=>angmax,"br_status"=>1,"index"=>i,"tap"=>1.0,"shift"=>0.0,
								"g_to"=>0.0,"g_fr"=>0.0,"b_to"=>0.0,"b_fr"=>0.0,
								"source_id"=>Any["branch", i], "transformer"=>false)
		else
			dict_temp=Dict("f_bus"=>row.fromNode,"t_bus"=>row.toNode,"br_r"=>r,"br_x"=>row.X,
								"angmin"=>angmin,"angmax"=>angmax,"br_status"=>1,"index"=>i,"tap"=>1.0,"shift"=>0.0,
								"g_to"=>0.0,"g_fr"=>0.0,"b_to"=>0.0,"b_fr"=>0.0,
								"rate_a"=>row.limit/1000, "rate_b"=>row.limit/1000, "rate_c"=>row.limit/1000,
								"source_id"=>Any["branch", i], "transformer"=>false)
		end
		edges[string(i)]=dict_temp
	end


	### sous partie load

	load=Dict{String,Any}()
	for i in 2:size(load_signal,1)
		bus_id=names(load_signal)[i]
		dict_temp=Dict("load_bus"=>parse(Int,bus_id),
								"source_id"=>Any["bus",parse(Int,bus_id)],
								"status"=>1,"pd"=>load_signal[bus_id]/1000,"qd"=>0,"index"=>i)
		load[string(i)]=dict_temp
	end

	dic_general=Dict{String,Any}();
	dic_general["bus"]=bus;
	dic_general["gen"]=gen;
	dic_general["branch"]=edges;
	dic_general["load"]=load;
	dic_general["shunt"]=Dict{String,Any}();
	dic_general["storage"]=Dict{String,Any}();
	dic_general["switch"]=Dict{String,Any}();
	dic_general["dcline"]=Dict{String,Any}();
	dic_general["per_unit"]=true;
	dic_general["baseMVA"]=1;

	max_gen=maximum(parse.(Int,collect(keys(dic_general["gen"]))))

	for i in 2:size(solar,1)-1
		bus_id=names(solar)[i]
		# display(bus_id)
		dict_temp=Dict("shutdown"=>0.0,"startup"=>0,"model"=>model,"cost"=>[0,0],
								"qmin"=>qmin,"qmax"=>qmax,"pmin"=>pmin/1000,"pmax"=>solar[bus_id]/1000,"gen_status"=>1,
								"gen_bus"=>parse(Int,bus_id),"source_id"=>Any["gen",i+max_gen],"index"=>i+max_gen, "ncost"=>2);
		gen[string(i+max_gen)]=dict_temp
	end

	max_gen_plus_solar=maximum(parse.(Int,collect(keys(dic_general["gen"]))))

	for i in 2:size(wind,1)-1
		bus_id=names(wind)[i]
		dict_temp=Dict("shutdown"=>0.0,"startup"=>0,"model"=>model,"cost"=>[0,0],
								"qmin"=>qmin,"qmax"=>qmax,"pmin"=>pmin/1000,"pmax"=>wind[bus_id]/1000,"gen_status"=>1,
								"gen_bus"=>parse(Int,bus_id),"source_id"=>Any["gen",i+max_gen_plus_solar],"index"=>i+max_gen_plus_solar, "ncost"=>2);
		gen[string(i+max_gen_plus_solar)]=dict_temp
	end

	dic_general
end

function create_dict_1888rte(solar::DataFrameRow, wind::DataFrameRow)
	file = "matpower/case1888rte.m"
	dic_1888rte = PowerModels.parse_matpower(file)

	model=2
	qmin=-Inf
	qmax=Inf
	pmin=0
	
	max_gen=maximum(parse.(Int,collect(keys(dic_1888rte["gen"]))))

	for i in 2:size(solar,1)-1
		bus_id=names(solar)[i]
		# display(bus_id)
		dict_temp=Dict("shutdown"=>0.0,"startup"=>0,"model"=>model,"cost"=>[0,0],"qmin"=>qmin,"qmax"=>qmax,"pmin"=>pmin,"pmax"=>solar[bus_id],"gen_status"=>1,
								"gen_bus"=>parse(Int,bus_id),"source_id"=>Any["gen",i+max_gen],"index"=>i+max_gen);
		dic_1888rte["gen"][string(i+max_gen)]=dict_temp
	end

	max_gen_plus_solar=maximum(parse.(Int,collect(keys(dic_1888rte["gen"]))))

	for i in 2:size(wind,1)-1
		bus_id=names(wind)[i]
		dict_temp=Dict("shutdown"=>0.0,"startup"=>0,"model"=>model,"cost"=>[0,0],"qmin"=>qmin,"qmax"=>qmax,"pmin"=>pmin,"pmax"=>wind[bus_id],"gen_status"=>1,
								"gen_bus"=>parse(Int,bus_id),"source_id"=>Any["gen",i+max_gen_plus_solar],"index"=>i+max_gen_plus_solar);
		dic_1888rte["gen"][string(i+max_gen_plus_solar)]=dict_temp
	end
	dic_1888rte
end