using CSV, DataFrames
using Dates

function get_renewables(;r_pen::Float64=1.0, layout_type::String="Uniform", model::String="ECMWF")
	# r_pen = renewables penetration in the entire network
	@assert layout_type ∈ ["Uniform", "Proportional"]
	@assert model ∈ ["ECMWF", "COSMO"]

	########################################################################################################
	## Solar 
	########################################################################################################
	solar_layout = CSV.read(string("RE-Europe_dataset_package/Metadata/solar_layouts_",model,".csv"),DataFrame)
	solar_layout.Proportional = solar_layout.Proportional .* r_pen
	solar_layout.Uniform = solar_layout.Uniform .* r_pen
	capacities = select(solar_layout, ["node", layout_type])

	solar_data_temp = CSV.read(string("RE-Europe_dataset_package/Nodal_TS/solar_signal_",model,".csv"),DataFrame)
	solar_data_temp.date = map(x->DateTime(x, DateFormat("y-m-d H:M:S")), solar_data_temp.Time)
	solar_data = deepcopy(solar_data_temp[1:end-1,:])

	for row in eachrow(capacities)
		nid = row.node
		solar_data[:,string(nid)] .= solar_data[:,string(nid)] .* row[2]
	end

	# ########################################################################################################
	# ## Wind
	# ########################################################################################################
	wind_layout = CSV.read(string("RE-Europe_dataset_package/Metadata/wind_layouts_",model,".csv"),DataFrame)
	wind_layout.Proportional = wind_layout.Proportional .* r_pen
	wind_layout.Uniform = wind_layout.Uniform .* r_pen
	capacities = select(wind_layout, ["node", layout_type])

	wind_data_temp = CSV.read(string("RE-Europe_dataset_package/Nodal_TS/wind_signal_",model,".csv"),DataFrame)
	wind_data_temp.date = map(x->DateTime(x, DateFormat("y-m-d H:M:S")), wind_data_temp.Time)
	wind_data = deepcopy(wind_data_temp[1:end-1,:])

	for row in eachrow(capacities)
		nid = row.node
		wind_data[:,string(nid)] .= wind_data[:,string(nid)] .* row[2]
	end

	solar_data, wind_data
end

function get_forecast(date::String;r_pen::Float64=1.0, layout_type::String="Uniform", model::String="ECMWF")
	# r_pen = renewables penetration in the entire network
	@assert layout_type ∈ ["Uniform", "Proportional"]
	@assert model ∈ ["ECMWF", "COSMO"]

	########################################################################################################
	## Solar 
	########################################################################################################
	solar_layout = CSV.read(string("RE-Europe_dataset_package/Metadata/solar_layouts_",model,".csv"),DataFrame)
	solar_layout.Proportional = solar_layout.Proportional .* r_pen
	solar_layout.Uniform = solar_layout.Uniform .* r_pen
	capacities = select(solar_layout, ["node", layout_type])

	solar_data_temp = CSV.read(string("RE-Europe_dataset_package/Nodal_FC/",date,"/solar_forecast.csv"),DataFrame)
	solar_data_temp.date = map(x->DateTime(x, DateFormat("y-m-d H:M:S")), solar_data_temp.Time)
	solar_data = deepcopy(solar_data_temp[1:end,:])

	for row in eachrow(capacities)
		nid = row.node
		solar_data[:,string(nid)] .= solar_data[:,string(nid)] .* row[2]
	end

	# ########################################################################################################
	# ## Wind
	# ########################################################################################################
	wind_layout = CSV.read(string("RE-Europe_dataset_package/Metadata/wind_layouts_",model,".csv"),DataFrame)
	wind_layout.Proportional = wind_layout.Proportional .* r_pen
	wind_layout.Uniform = wind_layout.Uniform .* r_pen
	capacities = select(wind_layout, ["node", layout_type])

	wind_data_temp = CSV.read(string("RE-Europe_dataset_package/Nodal_FC/",date,"/wind_forecast.csv"),DataFrame)
	wind_data_temp.date = map(x->DateTime(x, DateFormat("y-m-d H:M:S")), wind_data_temp.Time)
	wind_data = deepcopy(wind_data_temp[1:end,:])

	for row in eachrow(capacities)
		nid = row.node
		wind_data[:,string(nid)] .= wind_data[:,string(nid)] .* row[2]
	end

	solar_data, wind_data
end