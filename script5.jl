using CSV
using DataFrames
using PowerModels
using Ipopt
using StatsPlots
using JLD
using InlineStrings
using MathOptInterface

include("fonctions/renewables.jl")
include("fonctions/pre_processing.jl")
include("fonctions/visualization.jl")

load_signal=CSV.read("RE-Europe_dataset_package/Nodal_TS/load_signal.csv",DataFrame)
load_signal = load_signal[1:end-1, :] # pour récupérer le même nombre de points que solar et wind


## Chargement de la table de correspondance
table_correspondance = load(
	"resultats/OPF_effectif/table_correspondance.jld", 
	"table_correspondance")
table_correspondance= table_correspondance[1:100,:]#changer le 100 en 12338 pour faire pour tout les points
table_correspondance.optimal=[true for i=1:size(table_correspondance)[1]]
for i in 1:size(table_correspondance)[1]
	local result=load(string("resultats/OPF_effectif/nid",i,".jld"),"result")
	nb_true=0
	nb_false=0
	if result["termination_status"]==LOCALLY_SOLVED
		table_correspondance.optimal[i]=true
		nb_true=nb_true+1
	else
		table_correspondance.optimal[i]=false
		nb_false=nb_false+1
	end
end

# # # for i in 1:size(solar,1)
# # for i in 1:1
# # 	dic_general = create_dict(solar[i,:],wind[i,:],load_signal[i,:])
# # 	result = run_opf(dic_general,PowerModels.ACPPowerModel,Ipopt.Optimizer)
# # 	nid = table_correspondance[table_correspondance.time .== solar[i,:Time], :nid][1]
# # 	new_name = "nid$nid"
# # 	save(string("resultats/OPF_effectif/",new_name,".jld"),"result",result)
# # 	println("Result save!")
# # end

# optimizer = optimizer_with_attributes(Ipopt.Optimizer, "print_level"=>0)

# list_FC = readdir("RE-Europe_dataset_package/Nodal_FC")
# for i in 1:1030  											# jusqu à 2013-05-29
# 	dirname = list_FC[i]
# 	println("FC ", dirname)
# 	mkdir(string("resultats/OPF_FC/", dirname))
# 	solar_forecast, wind_forecast = get_forecast(dirname)
# 	for j in 1:size(solar_forecast,1)
# 		try
# 			d = solar_forecast.Time[j]
# 			dstring = table_correspondance[table_correspondance.time .== d, :nid][1]
# 			println("		", dstring)
# 			dic_general = create_dict(solar_forecast[j,:],wind_forecast[j,:],DataFrameRow(load_signal[(load_signal.Time.==d),:],1))
# 			result = run_opf(dic_general,PowerModels.ACPPowerModel,optimizer)
# 			save(string("resultats/OPF_FC/", dirname, "/nid", dstring,".jld"), "result", result)
# 		catch ex
# 			println("Error at $j th iteration.")
# 			# rethrow(ex)
# 		end
# 	end
# 	println("Result save!")
# end