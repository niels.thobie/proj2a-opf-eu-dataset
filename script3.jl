using CSV
using DataFrames
using PowerModels
using Ipopt
using StatsPlots
using JLD
using MathOptInterface

include("fonctions/renewables.jl")
include("fonctions/pre_processing.jl")
include("fonctions/visualization.jl")

# load_signal=CSV.read("RE-Europe_dataset_package/Nodal_TS/load_signal.csv",DataFrame)

# solar, wind = get_renewables()
# solar_forecast, wind_forecast = get_forecast("2012010100")
# save("resultats/test.jld","test",1)
# for i in 1:size(solar,1)
# # for i in 5:5
# 	dic_general = create_dict(solar[i,:],wind[i,:],load_signal[i,:])
# 	result = run_opf(dic_general,PowerModels.ACPPowerModel,Ipopt.Optimizer)
# 	jldopen("resultats/test.jld","r+") do fid 
# 		# write(fid,solar[i,1],result)
# 		fid[string(solar[i,1])]=result
# 	end
# 	println("Result save!")
# end

# for i in 1:size(solar_forecast,1)
# # for i in 1:2 #test pour le premier jour à 0h et 12h
# 	solar_forecast, wind_forecast = get_forecast(readdir("RE-Europe_dataset_package/Nodal_FC")[i])
# 	save(string("resultats/",readdir("RE-Europe_dataset_package/Nodal_FC")[i],"/result.jld"),"test",1)
# 	for j in 1:size(solar_forecast,1)
# 	# for j in 1:3  #test pour les 3 premières prédictions
# 		d=solar_forecast.Time[j]
# 		display(d)
# 		dic_general = create_dict(solar_forecast[j,:],wind_forecast[j,:],DataFrameRow(load_signal[(load_signal.Time.==d),:],1))
# 		result = run_opf(dic_general,PowerModels.ACPPowerModel,Ipopt.Optimizer)
# 		jldopen(string("resultats/",readdir("RE-Europe_dataset_package/Nodal_FC")[i],"/result.jld"),"r+") do fid 
# 			fid[string(solar_forecast[j,1])]=result
# 		end
# 	end
# 	println("Result save!")
# end

# result_all_time=JLD.load("resultats/image_test/test.jld")
delete!(result_all_time,"test")
equi=DataFrame(nid=1:length(result_all_time),time=collect(keys(result_all_time)))
for (k,v) in result_all_time
	dataframe_filename=equi[equi.time.==k,:]
	namefile=string(dataframe_filename.nid[1])
	visualize_voltage_angle_position(v,namefile)
end

# visualize_voltage(result)
# visualize_voltage_angle_position(result)
# visualize_power_position(dic_general, result)
# pm = instantiate_model(dic_general, ACPPowerModel, PowerModels.build_opf)
# ref = pm.ref[:it][:pm][:nw][0]

# file = "matpower/case57.m"
# dic = PowerModels.parse_matpower(file)
# pm0 = instantiate_model(dic, ACPPowerModel, PowerModels.build_opf)
# ref0 = pm0.ref[:it][:pm][:nw][0]
# result0 = run_opf(dic, PowerModels.ACPPowerModel, Ipopt.Optimizer)

# function visualize_pmaxdiff(dict_in::Dict, ref::Dict, result::Dict)
# 	bus = []
# 	diffs = Number[]

# 	for (key, value) in ref[:gen]
# 		diff = value["pmax"] - result["solution"]["gen"][string(key)]["pg"]
# 		push!(bus, key)
# 		push!(diffs, diff)
# 	end

# 	bar(diffs)
# 	# diffs
# end
# visualize_pmaxdiff(dic_general, ref, result)

# function visualize_prodcons(dict_in::Dict, ref::Dict, result::Dict)

# 	active_gen = 0.0
# 	reactive_gen = 0.0
# 	active_cons = 0.0
# 	reactive_cons = 0.0

# 	for (key,value) in ref[:gen]
# 		res_gen = result["solution"]["gen"][string(key)]
# 		active_gen += res_gen["pg"]
# 		reactive_gen += res_gen["qg"]
# 	end

# 	for (key,value) in ref[:load]
# 		active_cons += value["pd"]
# 		reactive_cons += value["qd"]
# 	end

# 	x = ["Active gen", "Active cons", "Reactive gen", "Reactive cons"]
# 	y = [active_gen, active_cons, reactive_gen, reactive_cons]

# 	bar(x,y)
# end
# visualize_prodcons(dic_general, ref, result)