using CSV
using DataFrames
using PowerModels
using Ipopt
using StatsPlots

include("fonctions/renewables.jl")
include("fonctions/pre_processing.jl")
include("fonctions/visualization.jl")

#network_nodes = CSV.read("RE-Europe_dataset_package/Metadata/network_nodes.csv",DataFrame)
#network_edges = CSV.read("RE-Europe_dataset_package/Metadata/network_edges.csv",DataFrame)
#generator_info = CSV.read("RE-Europe_dataset_package/Metadata/generator_info.csv",DataFrame)
#load_signal = CSV.read("RE-Europe_dataset_package/Nodal_TS/load_signal.csv",DataFrame)

# solar, wind = get_renewables()
# dic_general = create_dict(solar[10,:],wind[10,:])
# result = run_opf(dic_general,PowerModels.ACPPowerModel,Ipopt.Optimizer)
# visualize_prodcons(dic_general, result)
# visualize_busprod(result)
# visualize_branches(dic_general, result)

# dic_temp=[]
# for i in 1:10
# 	push!(dic_temp,i)
# end

# tot_pd=0
# for (key,value) in dic_general["load"]
# 	if value["load_bus"] in dic_temp
# 		global tot_pd+=value["pd"];
# 	end
# end

# tot_pmax=0
# for (key, value) in dic_general["gen"]
# 	if value["gen_bus"] in dic_temp
# 		global tot_pmax+=value["pmax"];
# 	end
# end
# println(tot_pd)
# println(tot_pmax)


# file = "matpower/case1888rte.m"
# dic_1888rte = PowerModels.parse_matpower(file)
# dic_1888rte_with_ren = create_dict_1888rte(solar[10,:],wind[10,:])
# result_1888rte=run_opf(dic_1888rte,PowerModels.ACPPowerModel,Ipopt.Optimizer)
# result_1888rte_with_ren=run_opf(dic_1888rte_with_ren,PowerModels.ACPPowerModel,Ipopt.Optimizer)

 visualize_busprod_withandwith_ren(result_1888rte,result_1888rte_with_ren)

