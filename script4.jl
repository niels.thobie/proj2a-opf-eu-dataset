using CSV
using DataFrames
using PowerModels
using Ipopt
using StatsPlots
using JLD

include("fonctions/renewables.jl")
include("fonctions/pre_processing.jl")
include("fonctions/visualization.jl")

load_signal=CSV.read("RE-Europe_dataset_package/Nodal_TS/load_signal.csv",DataFrame)
load_signal = load_signal[1:end-1, :] # pour récupérer le même nombre de points que solar et wind
solar, wind = get_renewables()

## Création de la table de correspondance
table_correspondance = DataFrame(:nid=>1:size(solar,1),
									:time=>solar.Time)

save("resultats/OPF_effectif/table_correspondance.jld", 
	"table_correspondance", table_correspondance)

# for i in 1:size(solar,1)
for i in 1:1
	dic_general = create_dict(solar[i,:],wind[i,:],load_signal[i,:])
	result = run_opf(dic_general,PowerModels.ACPPowerModel,Ipopt.Optimizer)
	nid = table_correspondance[table_correspondance.time .== solar[i,:Time], :nid][1]
	new_name = "nid$nid"
	save(string("resultats/OPF_effectif/",new_name,".jld"),"result",result)
	println("Result save!")
end

# for i in 1:size(solar_forecast,1)
# # for i in 1:2 #test pour le premier jour à 0h et 12h
# 	solar_forecast, wind_forecast = get_forecast(readdir("RE-Europe_dataset_package/Nodal_FC")[i])
# 	save(string("resultats/",readdir("RE-Europe_dataset_package/Nodal_FC")[i],"/result.jld"),"test",1)
# 	for j in 1:size(solar_forecast,1)
# 	# for j in 1:3  #test pour les 3 premières prédictions
# 		d=solar_forecast.Time[j]
# 		display(d)
# 		dic_general = create_dict(solar_forecast[j,:],wind_forecast[j,:],DataFrameRow(load_signal[(load_signal.Time.==d),:],1))
# 		result = run_opf(dic_general,PowerModels.ACPPowerModel,Ipopt.Optimizer)
# 		jldopen(string("resultats/",readdir("RE-Europe_dataset_package/Nodal_FC")[i],"/result.jld"),"r+") do fid 
# 			fid[string(solar_forecast[j,1])]=result
# 		end
# 	end
# 	println("Result save!")
# end