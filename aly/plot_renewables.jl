using Plots, GR
using CSV, DataFrames
using Dates

include("renewables.jl")

gr()

Npoints = 72  		# Nombre d'heures à afficher
Npp = 10			# Nombre de centrales renouvelables à afficher
layout = "Proportional" 	# 'Uniform' ou 'Proportional'

solar, wind = get_renewables(	layout_type = layout,
								model = "COSMO",)

solar2 = solar[1:Npoints, :]
wind2 = wind[1:Npoints, :]

nids = names(solar2)[2:end-1]
limited_nids = nids[1:Npp]

x = solar2.date
y = Array(solar2[:,limited_nids])
p1 = Plots.plot(x,y,title="solar")

x = wind2.date
y = Array(wind2[:,limited_nids])
p2 = Plots.plot(x,y,title="wind")

Plots.plot(p1,p2,legend=false)